import { BasePart, PartPrototype } from "../parts/part";

export const Skeleton: {
  human: {
    maleParts?: {
      partGroup: string;
      side: null | 1 | 0;
      part: PartPrototype;
    }[];
    femaleParts?: {
      partGroup: string;
      side: null | 1 | 0;
      part: PartPrototype;
    }[];

    defaultParts?: { side: null | 1 | 0; part: PartPrototype }[];
    defaultFaceParts?: { side: null | 1 | 0; part: PartPrototype }[];
    defaultDecorativeParts?: { side: null | 1 | 0; part: PartPrototype }[];
  };
} = { human: {} };
