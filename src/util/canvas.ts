/**
 * Canvas drawing layer order; higher value is further up
 * @memberof module:da
 * @readonly
 * @enum {number}
 */
export const Layer = Object.freeze({
    BASE              : 0,
    BACK              : 1,
    FRONT             : 2,
    SHADING_FRONT     : 3,
    MALE_GENITALS     : 4,
    MIDRIFT           : 5,
    SHADING_MIDRIFT   : 6,
    ARMS              : 7,
    SHADING_ARMS      : 8,
    GENITALS          : 9, // Technically just breasts now.
    SHADING_GENITALS  : 10,
    BELOW_HAIR        : 11,
    SHADING_BELOW_HAIR: 12,
    HAIR              : 13,
    SHADING_HAIR      : 14,
    ABOVE_HAIR        : 15,
    EFFECTS           : 16,
    NUM_LAYERS        : 17
});

export const ShadingLayers = [
    Layer.SHADING_FRONT,
    Layer.SHADING_MIDRIFT,
    Layer.SHADING_ARMS,
    Layer.SHADING_GENITALS,
    Layer.SHADING_BELOW_HAIR,
    Layer.SHADING_HAIR
];

/**
 * Minimal distance drawing on canvas to eliminate seams
 * @type {number}
 */
export const seamWidth = 0.0;

/**
 * Get access to a canvas group DOM element, creating it if it doesn't exist
 * @memberof module:da
 * @param {(string|HTMLElement)} groupname Id of the canvas group holder, or the DOM element
 * @param {object} styleOverride Object holding canvas style overrides
 * @returns {HTMLElement} The canvas group holder HTML DOM element
 */
export function getCanvasGroup(groupname, styleOverride) {
    let groupObj = groupname;
    if (typeof groupname === "string") {
        groupObj = document.getElementById(groupname);
    } else {
        groupname = groupname.id;
    }

    const styles = Object.assign(
        {
            width : "500",
            height: "800",
            parent: groupObj,
        },
        styleOverride);

    // size so surroundings can respect our boundaries
    groupObj.style.width = styles.width + "px";
    groupObj.style.height = styles.height + "px";
    groupObj.style.textAlign = "left";

    // const group : HTMLCanvasElement[] = [];
    // create various canvas layers with the same override
    for (let layer = 0; layer <= Layer.NUM_LAYERS; ++layer) {
        const canvasName = groupname + layer;
        // only display final canvas
        let hideWorkingCanvas = (layer !== Layer.NUM_LAYERS) ? {"visibility": "hidden"} : null;
        getCanvas(canvasName, Object.assign({}, styles, {"z-index": layer}, hideWorkingCanvas));
    }
    return groupObj;
}


/**  Get a canvas DOM element with id=canvasName, generating it if necessary
 styleOverride is the additional/overriding css style object to apply over
 defaults
 likely, you'd want to define its location:

 styles = {
     position:"absolute",
     top:"10px",
     left:"10px",
     parent: document.getElementById("canvas_holder"),
     }
 * @memberof module:da
 */
export function getCanvas(canvasName: string | HTMLCanvasElement, styles): HTMLCanvasElement {
    // if given a canvas object, use it directly
    let canvas = (typeof canvasName === "string") ? document.getElementById(canvasName) : canvasName;
    if (!styles.hasOwnProperty("parent")) {
        styles.parent = document.createElement("div");
        document.body.appendChild(styles.parent);
        styles.parent.style.width = styles.width + "px";
        styles.parent.style.height = styles.height + "px";
    }

    const creatingCanvas = !canvas;

    if (creatingCanvas) {
        if (typeof canvasName !== "string") {
            throw new Error("First time creating canvas should pass in canvas holder id (string)");
        }
        canvas = document.createElement("canvas");
        canvas.id = canvasName;
    }

    const realCanvas = canvas as HTMLCanvasElement;

    // width and height have to be set on the DOM element rather than styled
    realCanvas.width = styles.width;
    realCanvas.height = styles.height;
    realCanvas.style.position = "absolute";

    // add the rest of the styling
    for (let s in styles) {
        if (!styles.hasOwnProperty(s)) {
            continue;
        }
        if (s === "width" || s === "height") {
        } else if (styles.hasOwnProperty(s)) {
            realCanvas.style[s] = styles[s];
        }
    }

    if (creatingCanvas) {
        // ensure is first child of parent
        styles.parent.insertBefore(realCanvas, styles.parent.firstChild);
    }

    return realCanvas;
}

/**
 * Hide a canvas group from view (but not delete it)
 * @memberof module:da
 * @param {(string|HTMLElement)} groupName Either the id of the group element or the element itself
 */
export function hideCanvasGroup(groupName) {
    const group = getCanvasHandle(groupName);
    group.style.display = "none";
}

/**
 * Show a canvas group from view
 * @memberof module:da
 * @param {(string|HTMLElement)} groupName Either the id of the group element or the element itself
 */
export function showCanvasGroup(groupName) {
    const group = getCanvasHandle(groupName);
    group.style.display = "block";
}

export function getCanvasHandle(canvasName) {
    if (typeof canvasName === "string") {
        return document.getElementById(canvasName);
    } else {
        return canvasName;  // assume canvas element passed in
    }

}
