---
layout: post
title:  "1.0.0 Preview"
date:   2016-08-11
categories: preview
---
Version 1.0.0 is coming soon. It took a complete rewrite, but doing so allowed for
many new features and will make it easier to develop in the future.
The incomplete 1.0.0 tester can be found [here]({{site.baseurl}}/compare.html)
(the infrastructure is mostly complete, just a lot of content needs reimplementing)

### New Features 
- Bodyparts are now separate entities and know how to draw themselves
	- allows (almost) any kind of mix and matching
	- users can create and/or mutate body parts easily
- Dimensions system replace the old physique API
	- more intuitive variables that correspond to real life units (rather than arbitrary)
	- direct and consistent mapping of cm to canvas units
- Layered canvases rather than rendering to 1 canvas

### Bodyparts
As a showcase of the capabilities of the new engine, we replace one of our legs with one covered
by fur and ends in a horse hoove:
![mixing bodyparts]({{site.baseurl}}/res/legfur.PNG)

This is accomplished by:
{% highlight javascript %}
var rightLegFur = new da.Part.RightLegFur();
var rightLeg = new da.Part.RightLegHuman({stroke:da.Part.fur.stroke, fill:da.Part.fur.fill});
var myHoof = new da.Part.RightHoofHorse({stroke:"black", fill:"#392613"});
var PC = new da.Player({
	// other initial values
	parts: [myHoof, rightLeg],
	decorativeParts: [rightLegFur],
});
{% endhighlight %}
You can also modify and replace bodyparts after the Player object gets created.
Non-humanoid shapes can also be produced just as easily as humanoid shapes (actually it'd be easier).

### Backwards compatibility
The physique system will be deprecated and will eventually phased out when enough
dimensions are defined to replace all its expressiveness. The way you draw the avatar
will also be changed (to preserve the current functionality of drawfigure while 
physique is still being phased out)

{% highlight javascript %}
// instead of working on an individual canvas
var canvas_holder = document.getElementById("player");
var canvas = da.getCanvas("player_avatar", {
	parent:canvas_holder,
	border:"1px solid black",
	width:900,
	height:1200,
});
da.drawfigure(canvas, PC);


// work on the canvas holder directly since we'll create multiple canvas layers
var canvasGroup = da.getCanvasGroup("player", {
	border:"1px solid black",
	width:900,
	height:1200,
});
da.draw(canvasGroup, PC);
{% endhighlight %}

Use da.draw instead of da.drawfigure