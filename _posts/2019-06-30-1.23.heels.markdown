---
layout: post
title:  "1.23 Heels"
date:   2019-06-30
categories: release
---

- more options for heels
    - separate parts for platform, base, strap, and heel
    - new "fancy" template with open back
    - reusable calculation functions
- locked variant of heels

![heels](https://i.imgur.com/e4qGDq6.png)

## Options

There are a lot of new options. It's best to experiment with them
in the playground rather than having me explain them here, so here's
a preview of the list of options:

![creation options](https://i.imgur.com/e0s6WOA.png)
